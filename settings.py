import os

class QiniuConf:
    ACCESS_KEY = os.getenv("ACCESS_KEY")
    SECRET_KEY = os.getenv("SECRET_KEY")
    BUCKET_NAME = os.getenv("BUCKET_NAME")
    BUCKET_DOMAIN = os.getenv("BUCKET_DOMAIN")
    
class Server:
    HOST_VALUE = os.getenv("SERVER_HOST")
    PORT_VALUE = os.getenv("SERVER_PORT") 
    
