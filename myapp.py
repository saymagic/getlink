from flask import Flask, g, request, render_template
from qiniu import io, rs, fop, conf
from settings import QiniuConf
from settings import Server
from common import save_photo
import urllib

app = Flask(__name__)
app.debug = True

conf.ACCESS_KEY = QiniuConf.ACCESS_KEY
conf.SECRET_KEY = QiniuConf.SECRET_KEY
BUCKET_NAME = QiniuConf.BUCKET_NAME
BUCKET_DOMAIN = QiniuConf.BUCKET_DOMAIN
SERVER_HOST = Server.HOST_VALUE
SERVER_PORT = int(Server.PORT_VALUE)

@app.teardown_request
def teardown_request(exception):
    if hasattr(g, 'db'): g.db.close()

@app.route('/')
def homepage():
    return render_template('index.html', domain=BUCKET_DOMAIN)

@app.route('/token', methods=['GET'])
def uptoken():
    policy = rs.PutPolicy(BUCKET_NAME)
    uptoken = policy.token()
    return '{"uptoken": "%s"}' % (uptoken)
@app.route('/ext', methods=['GET'])
def getlink():
    url = request.args.get('url')
    try:
        data = urllib.urlopen(url).read()
        return save_photo(data)
    except:
	    return 'none'

if __name__ == "__main__":
  app.run(host=SERVER_HOST, port=SERVER_PORT)
